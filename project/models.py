from django.db import models
from django.contrib.auth.models import User

class register(models.Model):
    GENDER_CHOICES = (
        ("Male",'Male'),
        ("Female",'Female'),
    )
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    date_of_birth = models.CharField(max_length=100,blank=True,)
    gender = models.CharField(max_length=10, choices = GENDER_CHOICES,null=True,blank=True)
    contact=models.IntegerField(blank=True, null=True)
    about=models.CharField(max_length=300, blank=True, null=True)
    profession=models.CharField(max_length=300, blank=True, null=True)
    country=models.CharField(max_length=300, blank=True, null=True)
    state=models.CharField(max_length=300, blank=True, null=True)
    city=models.CharField(max_length=300, blank=True, null=True)
    profile_pic = models.ImageField(upload_to='images/%Y/%m/%d',blank=True,null=True,default='no_image.png')
    cover_pic = models.ImageField(upload_to='images/%Y/%m/%d',blank=True, null=True)
    registred_on = models.DateTimeField(auto_now_add=True)
    changed_on = models.DateField(auto_now=True)
    is_online = models.BooleanField(default=False,null=True,blank=True)

    def __str__(self):
        return self.user.first_name


class Post(models.Model):
    user = models.ForeignKey(register, on_delete = models.CASCADE)
    content = models.TextField(blank=True)
    media = models.FileField(upload_to='posts/%Y/%m/%d',blank=True)
    upload_on = models.DateTimeField(auto_now_add = True)
    edit_on = models.DateField(auto_now=True)

    def __str__(self):
        return self.user.user.username

class activity(models.Model):
    user = models.ForeignKey(register, on_delete = models.CASCADE)
    content = models.TextField(blank=True)
    status = models.BooleanField(default=False)
    time = models.TimeField(auto_now_add=True)
    date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.user.user.first_name

class education(models.Model):
    user = models.ForeignKey(register, on_delete = models.CASCADE)
    company = models.CharField(max_length=200, blank=True)
    position = models.CharField(max_length=200, blank=True)
    work_city = models.CharField(max_length=200, blank=True)
    work_from = models.CharField(max_length=200, blank=True)
    work_to = models.CharField(max_length=200, blank=True)
    university = models.CharField(max_length=200, blank=True)
    stream = models.CharField(max_length=200, blank=True)
    uni_city = models.CharField(max_length=200, blank=True)
    uni_from = models.CharField(max_length=200, blank=True)
    uni_to = models.CharField(max_length=200, blank=True)
    school = models.CharField(max_length=200, blank=True)
    school_city = models.CharField(max_length=200, blank=True)
    school_from = models.CharField(max_length=200, blank=True)
    school_to = models.CharField(max_length=200, blank=True)


    def __str__(self):
        return self.user.user.first_name

class followUsers(models.Model):
    followers = models.ForeignKey(register, on_delete = models.CASCADE,related_name="followers")
    following = models.ForeignKey(register, on_delete = models.CASCADE,related_name="following")
    create_date = models.DateTimeField(auto_now_add = True)
    is_friend = models.BooleanField(default=False)

    def __str__(self):
        return self.followers.user.username

class notify(models.Model):
    user = models.ForeignKey(register, on_delete = models.CASCADE,related_name="to")
    frm =models.ForeignKey(register, on_delete = models.CASCADE,null=True,related_name="frm")
    content = models.CharField(max_length=100)
    add = models.DateTimeField(auto_now_add =  True) 
    status = models.BooleanField(default = False)

class inbox(models.Model):
    sender=models.ForeignKey(register, on_delete = models.CASCADE,related_name="sender")
    receiver = models.ForeignKey(register, on_delete = models.CASCADE,null=True,related_name="receiver")
    message=models.TextField()
    status=models.BooleanField(default=False)
    approved =models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class likes(models.Model):
    follower =models.ForeignKey(register,on_delete=models.CASCADE)
    postid = models.ForeignKey(Post,on_delete = models.CASCADE)
    create_date = models.DateTimeField(auto_now_add = True)
    is_like = models.BooleanField(default=False)

    def __str__(self):
        return repr(self.follower)

    class Meta():
        verbose_name_plural="Like Table"


class postcomment(models.Model):
    postid = models.ForeignKey(Post,on_delete=models.CASCADE,null=True)
    commenterid = models.ForeignKey(register,on_delete=models.CASCADE,null=True)
    comment = models.TextField()
    create_date = models.DateTimeField(auto_now_add = True)
    aproved= models.BooleanField(default=False)

    def __str__(self):
        return repr(self.postid)

class interest(models.Model):
    user = models.ForeignKey(register, on_delete = models.CASCADE)
    add_interest = models.CharField(max_length=100,blank=True)
    update_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.first_name

class contact(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField(max_length=200)
    number = models.IntegerField()
    message = models.CharField(max_length=200)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name